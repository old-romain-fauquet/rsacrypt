from math import sqrt
import random

"""Renvoi le pgcd de a et b"""
def pgcd(a,b) :  
   while a%b != 0 : 
      a, b = b, a%b 
   return b

"""Renvoi le plus grand nombre premier avec n"""
def premier_avec(n):
    if est_premier(n):
        return random.choice(liste_premier(n))
    while 1:
        x = random.randrange(n)
        if pgcd(x,n)==1:
            return x

"""True si n est premier, sinon False"""
def est_premier(n):
    if n<7:
        if n in (2,3,5):
            return True
        else:
            return False
    if n & 1 == 0:
        return False
    k=3
    r=sqrt(n)
    while k<=r:
        if n % k == 0:
            return False
        k+=2
    return True

"""Retourne la liste des premier jusque n"""
def liste_premier(n):
    nb_premier = [2]
    for i in range(3, n, 2):
        if est_premier(i):
            nb_premier.append(i)
    return nb_premier

"""Retourne le plus grand premier avant n"""
def plus_grand_premier(n):
    return liste_premier(n)[-1]
