# -*- coding: utf-8 -*-
from premier import *
from builtins import pow as powmod
import random
import getpass

"""Calcul le pgcd de a et b"""
def pgcd(a,b) :
   while a%b != 0 : 
      a, b = b, a%b 
   return b

"""Demande a l'utilisateur de saiir un nombre"""
def saisie(msg, cacher=False):
    if cacher:
        n = int(getpass.getpass(msg))
    else:
        n = int(input(msg))
    return n

"""Bezout : fonction recuperer sur un site, permet de determiner la clef prive d dans la creation de clef, voir doc rsa"""
def bezout_pure (a,b):
    r0=a 
    r1=b 
    u0=0 
    u1=1 
    v0=1 
    v1=-r0//r1 
    s=[r1,u0,v0] 
    while r0%r1: 
        r=r0 
        r0=r1 
        r1=r%r1 
        u=u0 
        v=v0 
        u0=u1 
        v0=v1 
        q=r0//r1 
        u1= u-q*u1 
        v1=v-q*v1 
        s=[r1,u0,v0] 
    return s

def Bezout(a,b):
    s = bezout_pure(a,b)[1]
    if s > 2:
        return s
    else:
        i=1
        while s < 2:
            s = s - i * b
            i=i+1
    return s

"""Donne les facteur possible de d afin de le determiner"""
def liste_facteur_d(d, p, q, phi_n):
    x = d
    mini = 1
    while x  < p and x < q:
        mini += 1
        x = x * mini
    x = mini + 1
    while d*x < phi_n:
        maxi = x
        x+=1
    maxi = x-1
    i = []
    for j in range(mini, maxi):
        i.append(j)
    return i

"""Genere un nombre hexadecimal aleatoire sur n*4 bit (range(n))"""
def generer_hexa():
    hexa="0123456789abcdef"
    r="0x"
    for i in range(10):
        r = r + random.choice(hexa)
    return int(r,16)

"""Genere un nombre premier"""
def generer_nombre():
    h = generer_hexa()
    if est_premier(h):
        return h
    return generer_nombre()

"""Genere les clef publique et privee, voir doc rsa"""
def creer_clef():
    p = generer_nombre()
    q = generer_nombre()
    n = p*q
    phi_n = (p-1) * (q-1)
    
    # genereation de e
    e = max([p,q]) + 1
    while pgcd(e,phi_n) != 1:
        e += 1
    if phi_n == e:
        print("Erreur ! Les nombres choisis sont trop petit !")
        return -1
    
    # generation de d
    d = abs(Bezout(e,phi_n))
    liste = liste_facteur_d(d, p, q, phi_n)
    x = liste[0]
    for i in liste:
        if e * d * i % phi_n == 1:
            x = i
            break
    d = d * x
    
    # Purement indicatif
    pub = (n,e)
    pri = (n,d)
    
    # Affiche les clefs prive
    print("n : " + str(hex(n)))
    print("publickey : " + str(hex(e)))
    print("privatekey : " + str(hex(d)))


""" Chiffre la chaine de caractere txt avec n et e couple de la clef publique """
def chiffrer(txt, n, e, fichier=False, sortie=""):
    if sortie != "":
        fichier_out=open(sortie,"w")
    if fichier:
        txt=open(txt,'r')
        txt=txt.read()
    pub = (int(n,16),int(e,16))
    taille = len(txt)
    i = 0
    while i < taille:
        asc = ord(txt[i])
        lettre_crypt = powmod(asc,pub[1],pub[0])
        if asc > pub[0]:
            print("Les nombres p et q sont trop petit, veuillez regenerez vos clefs.")
        if sortie == "":
            print(str(lettre_crypt) + " ", end='')
        else:
            if (i+1)%int(taille/100)==0:
                print(str(i) + "/" + str(taille) + "\t" + str(int((i*100)/taille)) + "%")
            fichier_out.write(str(lettre_crypt) + " ")
        i = i + 1
    if sortie != "":
        fichier_out.close()
    print()

""" Dechiffre un message avec n et d couple de la clef prive"""
def dechiffrer(block, n, d, fichier=False, sortie=""):
    if sortie != "":
        fichier_out=open(sortie,"w")
    if fichier:
        block=open(block,'r').read()
    priv = (int(n,16),int(d,16))
    b = block.split(" ")
    if b[-1] == "":
        b = b[:-1]
    compteur = 0
    s = ""
    while compteur < len(b):
        try:
            b[compteur] = int(b[compteur])
            asc = powmod(b[compteur], priv[1], priv[0])
            if sortie=="":
                print(chr(asc), end='')
            else:
                fichier_out.write(chr(asc))
                if (compteur+1)%int(len(b)/100)==0:
                    print(str(compteur) + "/" + str(len(b)) + "\t" + str(int((compteur*100)/len(b))) + "%")
        except:
            if sortie=="":
                print(str(b[compteur]), end='')
            else:
                if (compteur+1)%int(len(b)/100)==0:
                    print(str(compteur) + "/" + str(len(b)) + "\t" + str(int((compteur*100)/len(b))) + "%")
                fichier_out.write(str(b[compteur]))
        compteur = compteur + 1
    if sortie != "":
        fichier_out.close()
    print()

""" Permet d'executer le code """
if __name__=='__main__':
    import sys
    try:
        func = sys.argv[1]
    except:
        func = None
    if func:
        exec(func)
    else:
        print("Type help for documentation")
    exit()
